build:
	cross build --target=armv7-unknown-linux-gnueabihf

release:
	cross build --target=armv7-unknown-linux-gnueabihf --release

test:
	cross test --target=armv7-unknown-linux-gnueabihf

send: release
	scp ./target/armv7-unknown-linux-gnueabihf/release/tempsense pimon:~/.cargo/bin/

run: send
	ssh pimon '~/.cargo/bin/tempsense'

start: send
	ssh -t pimon 'sudo systemctl start tempsense.service'

stop:
	ssh -t pimon 'sudo systemctl stop tempsense.service'

restart: stop send start

journal: 
	ssh pimon 'journalctl -ft tempsense'

.PHONY: build release test send run start stop restart journal
