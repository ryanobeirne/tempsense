use super::*;
use clap::ArgMatches;
use serde_derive::Deserialize;
pub use std::convert::TryFrom;
use std::path::Path;
use std::io;

const UNKNOWN: &str = "UNKNOWN";

#[derive(Debug, Deserialize)]
pub struct HostConfig {
    pub hostname: String,
    pub location: String,
    pub factory: String,
}

#[test]
fn deserialize_host_config() -> io::Result<()> {
    let host_config = HostConfig::from_file("tempsense.toml")?;
    dbg!(&host_config);
    assert_eq!(host_config.hostname, "pimon");
    assert_eq!(host_config.location, "RyansDesk");
    assert_eq!(host_config.factory, "RDU");
    Ok(())
}

impl HostConfig {
    pub fn new(hostname: &str, location: &str, factory: &str) -> Self {
        HostConfig {
            hostname: hostname.into(),
            location: location.into(),
            factory: factory.into(),
        }
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> io::Result<HostConfig> {
        let toml_file = std::fs::read_to_string(path)?;
        Ok(toml::from_str(&toml_file)?)
    }
}

impl Default for HostConfig {
    fn default() -> Self {
        HostConfig {
            hostname: get_hostname().unwrap_or(UNKNOWN.to_string()),
            location: UNKNOWN.to_string(),
            factory: UNKNOWN.to_string(),
        }
    }
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for HostConfig {
    type Error = std::io::Error;
    fn try_from(matches: &ArgMatches) -> std::result::Result<Self, Self::Error> {
        if let Some(config_file) = matches.value_of("config-file") {
            return HostConfig::from_file(config_file);
        }

        let hostname = matches
            .value_of("hostname")
            .unwrap_or(get_hostname()?.as_str())
            .to_string();

        let (location, factory) = match (matches.value_of("location"), matches.value_of("factory"))
        {
            (Some(loc), Some(fac)) => (loc, fac),
            _ => return Err(std::io::Error::from(std::io::ErrorKind::UnexpectedEof)),
        };

        Ok(HostConfig::new(&hostname, location, factory))
    }
}

// Determine host name
pub fn get_hostname() -> std::io::Result<String> {
    Ok(var("HOST")
        .unwrap_or(
            var("HOSTNAME")
                .unwrap_or(hostname_cmd().unwrap_or(std::fs::read_to_string("/etc/hostname")?)),
        )
        .trim()
        .to_string())
}

// Run the `hostname` command
fn hostname_cmd() -> std::io::Result<String> {
    Ok(std::process::Command::new("hostname")
        .output()?
        .stdout
        .into_iter()
        .map(|u| u as char)
        .collect())
}
