use super::*;

#[derive(Debug)]
pub struct LogLine {
    time: DateTime<Local>,
    temp: Temp,
    pres: Pressure,
    humid: Humidity,
    //gas: Gas,
}

impl LogLine {
    pub fn new(time: DateTime<Local>, temp: f32, pres: f32, humid: f32 /*gas: u32*/) -> Self {
        LogLine {
            time: time,
            temp: Temp(temp),
            pres: Pressure(pres),
            humid: Humidity(humid),
            //gas: Gas(gas),
        }
    }

    pub async fn log_weather(
        &self,
        client: &Client,
        config: &HostConfig,
        time: Timestamp,
    ) -> std::result::Result<(), influxdb::Error> {
        let write_query = WriteQuery::new(time, WEATHER_DB)
            .add_tag("host", config.hostname.as_str())
            .add_tag("location", config.location.as_str())
            .add_tag("factory", config.factory.as_str())
            .add_field("temperature", self.temp.0)
            .add_field("pressure", self.pres.0)
            .add_field("humidity", self.humid.0);

        client.query(&write_query).await?;

        Ok(())
    }

    pub fn is_default(&self) -> bool {
        (self.temp == 31.34 || self.temp == 32.84)
            && (self.pres == 712.58 || self.pres == 714.3)
            && self.humid == 100.0
    }
}

impl fmt::Display for LogLine {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}\t{}\t{}\t{}",
            self.time.format("%Y/%m/%d %H:%M:%S"),
            self.temp.unit_display(),
            self.pres.unit_display(),
            self.humid.unit_display()
        )
    }
}

#[derive(Debug, PartialEq, PartialOrd)]
struct Temp(f32);

impl Temp {
    fn to_fahrenheit(&self) -> f32 {
        self.0 * (9.0 / 5.0) + 32.0
    }
}

#[derive(Debug, PartialEq, PartialOrd)]
struct Pressure(f32);

#[derive(Debug, PartialEq, PartialOrd)]
struct Humidity(f32);

#[derive(Debug, PartialEq, PartialOrd)]
struct Gas(u32);

impl PartialEq<f32> for Temp {
    fn eq(&self, other: &f32) -> bool {
        self.0 == *other
    }
}

impl PartialEq<f32> for Humidity {
    fn eq(&self, other: &f32) -> bool {
        self.0 == *other
    }
}

impl PartialEq<f32> for Pressure {
    fn eq(&self, other: &f32) -> bool {
        self.0 == *other
    }
}

trait UnitDisplay<T: fmt::Display> {
    const UNITS: &'static str;
    fn inner_value(&self) -> &T;
    fn unit_display(&self) -> String {
        format!("{}{}", self.inner_value(), Self::UNITS)
    }
}

impl fmt::Display for Temp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl UnitDisplay<f32> for Temp {
    const UNITS: &'static str = "°C";
    fn inner_value(&self) -> &f32 {
        &self.0
    }
}

impl fmt::Display for Humidity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl UnitDisplay<f32> for Humidity {
    fn inner_value(&self) -> &f32 {
        &self.0
    }
    const UNITS: &'static str = "%RH";
}

impl fmt::Display for Pressure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl UnitDisplay<f32> for Pressure {
    fn inner_value(&self) -> &f32 {
        &self.0
    }
    const UNITS: &'static str = "hPa";
}

impl fmt::Display for Gas {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl UnitDisplay<u32> for Gas {
    fn inner_value(&self) -> &u32 {
        &self.0
    }
    const UNITS: &'static str = "Ω";
}
