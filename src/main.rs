extern crate linux_embedded_hal as hal;
#[macro_use]
extern crate clap;

use bme680::{
    Bme680,
    Error,
    FieldData,
    FieldDataCondition,
    I2CAddress,
    IIRFilterSize,
    OversamplingSetting,
    PowerMode,
    Settings,
    SettingsBuilder,
};

use chrono::*;
use embedded_hal::blocking::i2c;
use hal::{Delay, I2cdev};
use influxdb::{Client, WriteQuery, Timestamp};

use std::{
    env::var,
    fmt,
    result,
    time::Duration,
};

mod cli;

mod logline;
use logline::*;

mod hostconfig;
use hostconfig::*;

const INFLUX_DB: &str = "http://172.16.19.10:8086";
const WEATHER_DB: &str = "weather";
const I2C_PRIMARY: &str = "/dev/i2c-1";
const I2C_SECONDARY: &str = "/dev/i2c-2";

//type BoxErr = Box<dyn std::error::Error>;
//type BoxResult<T> = std::result::Result<T, BoxErr>;

type HalErr = Error<<I2cdev as i2c::Read>::Error, <I2cdev as i2c::Write>::Error>;
type HalResult<T> = result::Result<T, HalErr>;

#[tokio::main]
async fn main() -> HalResult<()> {
    // Initialize device
    let mut dev = Device::init()?;

    // Get command line arguments
    let matches = cli::app().get_matches();
    let hostconfig = HostConfig::try_from(&matches)
        .unwrap_or(HostConfig::from_file("/etc/tempsense.toml")
        .unwrap_or_default());
    let influx_db = matches.value_of("influxdb").unwrap_or(INFLUX_DB);
    let client = Client::new(influx_db, WEATHER_DB);

    loop {
        sleep(2000);

        let now = Local::now();
        let timestamp = Timestamp::Seconds(now.timestamp() as u128);
        dev.set_sensor_mode()?;
        // Read sensor data
        let (data, _state) = dev.get_sensor_data()?;
        let logline = LogLine::new(
            now,
            data.temperature_celsius(),
            data.pressure_hpa(),
            data.humidity_percent(),
            //data.gas_resistance_ohm(),
        );

        // Check that the device has not come unplugged
        if logline.is_default() {
            eprintln!("Measurement device failure!");
            dev = Device::reinit();
            continue;
            //std::process::exit(1);
        }

        match logline.log_weather(&client, &hostconfig, timestamp).await {
            Ok(_) => println!("{}", &logline),
            Err(e) => eprintln!("{}", e),
        }
    }

    //Ok(())
}

fn sleep(ms: u64) {
    std::thread::sleep(Duration::from_millis(ms))
}

struct Device {
    dev: Bme680<hal::I2cdev, hal::Delay>,
}

impl Device {
    fn init() -> HalResult<Self> {
        let mut dev =
            Device {
                dev: Bme680::init(get_i2c(), Delay {}, I2CAddress::Primary)
                    .unwrap_or(Bme680::init(get_i2c(), Delay {}, I2CAddress::Secondary)?),
            };

        dev.set()?;

        Ok(dev)
    }

    fn reinit() -> Self {
        match Device::init() {
            Ok(dev) => dev,
            Err(_) => {
                eprintln!("Unable to initialize BME680!");
                sleep(2000);
                let mut device = Device::reinit();
                match device.set() {
                    Ok(_) => device,
                    Err(_) => {
                        eprintln!("Unable to set BME680 settings!");
                        Device::reinit()
                    }
                }
            }
        }
    }

    fn set_sensor_mode(&mut self) -> HalResult<()> {
        self.dev.set_sensor_mode(PowerMode::ForcedMode)
    }

    fn set(&mut self) -> HalResult<()> {
        self.dev.set_sensor_settings(Device::settings())?;
        self.set_sensor_mode()
    }

    fn get_sensor_data(&mut self) -> HalResult<(FieldData, FieldDataCondition)> {
        self.dev.get_sensor_data()
    }

    fn settings() -> Settings {
        SettingsBuilder::new()
            .with_humidity_oversampling(OversamplingSetting::OS8x)
            .with_pressure_oversampling(OversamplingSetting::OS8x)
            .with_temperature_oversampling(OversamplingSetting::OS8x)
            .with_temperature_filter(IIRFilterSize::Size3)
            .with_temperature_offset(-1.5)
            //.with_gas_measurement(Duration::from_millis(1500), 320, 25)
            //.with_run_gas(true)
            .build()
    }
}

fn get_i2c() -> I2cdev {
    match I2cdev::new(I2C_PRIMARY) {
        Ok(i2c) => i2c,
        Err(e) => {
            eprintln!("{}", e);
            I2cdev::new(I2C_SECONDARY).expect("i2c device not found!")
        }
    }
}
