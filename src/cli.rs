use clap::{App, Arg};

pub fn app() -> App<'static, 'static> {
    App::new("tempsense")
        .author(crate_authors!())
        .version(crate_version!())
        .arg(
            Arg::with_name("config-file")
                .help("Path to configuration TOML file")
                .long("config-file")
                .alias("config")
                .short("c")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("factory")
                .help("Specify factory name")
                .long("factory")
                .short("f")
                .possible_values(&["brl", "rdu"])
                .required_unless("config-file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("location")
                .help("Specify device location (e.g. PrintRoom)")
                .long("location")
                .short("l")
                .required_unless("config-file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("hostname")
                .help("Override this measurement device hostname")
                .long("hostname")
                .alias("host")
                .short("h")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("influxdb")
                .help("Specify influxdb URL")
                .long("influxdb")
                .short("i")
                .takes_value(true),
        )
}
